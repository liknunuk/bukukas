<?php
// sesuaikan nama kelas, tetap extends ke Controller
class KasWoong extends Controller
{
  // method default
  public $activeMenu;

  public function __construct(){
    if(!isset($_SESSION) || $_SESSION['loggedIn']==false ){
      header("Location:" . BASEURL );
    }
  }

  public function index($cp=1,$kp=1)
  {
    // cp = nomor halaman coa
    // kp = nomor halaman buku kas
    $tanggal = date('Y-m-d');
    $data['cp'] = $cp;
    $data['kp'] = $kp;
    $data['coa'] = $this->model('Model_coa')->tampil($cp);
    $data['trx'] = $this->model('Model_kbWoong')->transaksiHarian($tanggal);
    $data['controller'] = 'KasWoong';

    $this->activeMenu = 'index';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('bukukas/kas-navbar',$data);
    $this->view('bukukas/indexWoong',$data);
    $this->view('template/footer');
  }

  public function simpan(){
      if($this->model('Model_kbWoong')->tambah($_POST) > 0 ){
          Alert::setAlert('berhasil disimpan' , 'Transaksi harian' , 'success');
        }else{          
          Alert::setAlert('gagal disimpan' , 'Transaksi harian' , 'warning');
      }
      header("Location:" . BASEURL ."KasWoong");
  }

  // chart of account

  public function coa($pn=1){
    
    $data['coa'] = $this->model('Model_coa')->tampil($pn);

    $this->activeMenu = 'coa';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('bukukas/kas-navbar');
    $this->view('bukukas/coa',$data);
    $this->view('template/footer');
  }

  public function setCoa(){
    if( $this->model('Model_coa')->tambah($_POST) > 0 ){
      Alert::setAlert('berhasil ditamahkan','Chart of Account','success');
    }else{
      Alert::setAlert('gagal ditamahkan','Chart of Account','danger');
    }
    header("Location:" . BASEURL ."KasWoong");
  }

  public function chgCoa(){
    if( $this->model('Model_coa')->ngubah($_POST) > 0 ){
      Alert::setAlert('berhasil dimutakhirkan','Chart of Account','success');
    }else{
      Alert::setAlert('gagal dimutakhirkan','Chart of Account','danger');
    }
    header("Location:" . BASEURL ."KasWoong/coa");
  }

  public function rmvCoa(){
    if( $this->model('Model_coa')->sampah($_POST) > 0 ){
      echo "1";
    }else{
      echo "0";
    }
  }

  public function rekap($tahun="",$bulan=""){
    $periode = $bulan=="" && $tahun=="" ? date('Y-m') : "{$tahun}-{$bulan}";
    $data['remo'] = $this->model('Model_kbWoong')->rekapBulanan($periode);
    $data['reta'] = $this->model('Model_kbWoong')->rekapTahunan($tahun);
    $data['bulan'] = $bulan;
    $data['tahun'] = $tahun;
    $data['controller'] = 'BANYUWOONG';
    $this->activeMenu = 'rekap';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('bukukas/kas-navbar');
    $this->view('bukukas/recap',$data);
    $this->view('template/footer');
  }

  public function caritrx(){
    $this->activeMenu = 'cari';
    $data['trx'] = $this->model('Model_kbWoong')->cariTrx($_POST['col'],$_POST['key']);
    
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('bukukas/kas-navbar');
    $this->view('bukukas/hasilcari',$data);
    $this->view('template/footer');
    unset($_POST);
  }

  public function isActive($string){
    if($string == $this->activeMenu) {
        return "active";
    }
  }

  public function dmy($tanggal){
      list($t,$b,$h) = explode("-",$tanggal);
      return "$h/$b/$t";
  }
//   Private functions
}
