<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $this->view('template/header');
    // $this->view('template/pageHeader');
    $this->view('forms/login');
    $this->view('template/footer');
  }
  public function auth(){
    $data = $this->model('Model_persons')->authenticate($_POST);
    // Array ( [userId] => 100 [fullName] => Raden Mas Nugroho [email] => nugroho.redbuff@gmail.com [tempPassword] => 675db0b562d91bba47a45be8d706ec4b [isadmin] => 1 )
    if( count($data) <=1 ){
      Alert::setAlert("Tidak Ditemukan" , "Data User" , "danger" );
      header("Location:".BASEURL);
    }else{
      if( $data['isadmin'] == "1") {
        $_SESSION['admin']=true;
              }else{
        $_SESSION['admin']=false;        
      }
      $_SESSION['loggedIn'] = true;
      $_SESSION['user'] = $data['fullName'];
    }
    header("Location: " . BASEURL . "Bukukas");
  }

  public function metusikah(){
    session_unset();
    session_destroy();
    header("Location:" . BASEURL );
  }
}
