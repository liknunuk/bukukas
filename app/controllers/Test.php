<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Test extends Controller
{
  // method default
  public function index()
  {
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('template/navbar');
    $this->view('home/test');
    $this->view('template/footer');
  }
}
