<div class="row">
    <div class="col-lg-6 mx-auto">
        <div class="page-title">
            <h3>Chart of Account</h3>
        </div>
        <?php Alert::sankil(); ?>
        <table class="responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Arti</th>
                        <th>Kontrol</th>
                    </tr>
                    <form action="<?=BASEURL;?>Bukukas/setcoa" method="post">
                        <tr>
                            <td>
                                <input type="number" name="kode" id="kode" class="form-control">
                            </td>
                            <td>
                                <input type="text" name="arti" id="arti" class="form-control">
                            </td>
                            <td>
                                <input type="submit" value="Simpan" class="btn btn-primary">
                            </td>
                        </tr>
                    </form>
                </thead>
                <tbody id="listOfCoa">
                    <?php foreach($data['coa'] as $coa):?>
                    <tr>
                        <td><?=$coa['kode'];?></td>
                        <td><?=$coa['arti'];?></td>
                        <td width='120' class='text-center'>
                            <a href="javascript:void(0)" class='coaEdit'>
                                <i class="fas fa-edit icon48"></i>
                            </a>
                            <a href="javascript:void(0)" class='coaRmv col-red'>
                                <i class="fas fa-times icon48"></i>
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </table>
    </div>
</div>

<!-- modals -->

<!-- Modal COA -->
<div class="modal" tabindex="-1" role="dialog" id="modalCoa">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Chart Of Account</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>Bukukas/chgcoa" method="post" class="form-horizontal">
            <div class="form-group row">
                <label for="mdckode" class="col-sm-3">Kode Coa</label>
                <div class="col-sm-9">
                    <input type="number" name="kode" id="mdckode" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="mdcarti" class="col-sm-3">Arti</label>
                <div class="col-sm-9">
                    <input type="text" name="arti" id="mdcarti" class="form-control" maxlength="45" placeHolder="info singkat" required>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- modals -->

<?php $this->view('template/bs4js'); ?>
<script>
$('.coaEdit').on('click', function(){
    let kode,arti;
    kode=$(this).parent().parent().children('td:nth-child(1)').text();
    arti=$(this).parent().parent().children('td:nth-child(2)').text();
    $('#modalCoa').modal('show');
    $('#mdckode').val(kode);
    $('#mdcarti').val(arti);
})

$('.coaRmv').on('click',function(){
    let tenan = confirm('Data akan dihapus!');
    if( tenan == true ){
        let kode=$(this).parent().parent().children('td:nth-child(1)').text();
        $.post('<?=BASEURL;?>Bukukas/rmvCoa' , { kode: kode } , function(resp){
            if( resp == '1' ){
                location.reload();
            }
        })
    }
})
</script>