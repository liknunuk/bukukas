<?php if(!isset($_POST)) : ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <form action="<?=BASEURL;?>Bukukas/caritrx" method="post" class="form-horizontal">
            
            <div class="form-group row">
                <label for="txsrccol" class="col-sm-4">Parameter</label>
                <div class="col-sm-8">
                    <input type="text" name="col" id="txsrccol" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="txsrckey" class="col-sm-4">Parameter</label>
                <div class="col-sm-8">
                    <input type="text" name="key" id="txsrckey" class="form-control">
                </div>
            </div>
        </form>
    </div>
</div>
<?php endif; ?>
<div class="row">
    <div class="col-md-12">
        <div class="page-title">
            <h3>Hasil Pencarian Transaksi</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Uraian</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                    </tr>
                </thead>
                <tbody id="listOfTrx">
                <?php foreach($data['trx'] as $trx): ?>
                    <tr>
                        <td><?=$this->dmy($trx['tanggal']);?></td>
                        <td>
                            <div class='trxNote'><?=$trx['kode'].'-'.$trx['arti'];?></div>
                            <div><?=$trx['keterangan'];?></div>
                        </td>
                        <td class="text-right"><?=number_format($trx['debet'],2,',','.');?></td>
                        <td class="text-right"><?=number_format($trx['kredit'],2,',','.');?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>