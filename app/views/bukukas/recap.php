<div class="row">
    <div class="col-md-12">
        <div class="page-title">
            <h3>REKAPITULASI KEUANGAN <?=$data['controller'];?></h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="page-title">
            <h3>Rekap Bulanan</h3>
        </div>

        <div class="form-group row">
            <?php 
                $tahun = date('Y'); 
                $bulan = [
                    '01'=>'Januari',
                    '02'=>'Februari',
                    '03'=>'Maret',
                    '04'=>'April',
                    '05'=>'Mei',
                    '06'=>'Juni',
                    '07'=>'Juli',
                    '08'=>'Agustus',
                    '09'=>'September',
                    '10'=>'Oktober',
                    '11'=>'Nopember',
                    '12'=>'Desember'
                ];
            ?>
            <div class="col-md-4">
                <select id="rkBulan" class="form-control">
                    <option value="">Pilih Bulan</option>
                    <?php foreach($bulan as $a=>$b): ?>
                    <option value="<?="{$tahun}/{$a}";?>"><?="{$b} {$tahun}";?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-8">
                <h4>Bulan <?=$bulan[ $data['bulan'] ] . " " . $tahun;?> </div></h4>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Uraian</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                    </tr>
                </thead>
                <tbody id="listOfRemo">
                    <?php 
                        $totalDebet=0;
                        $totalKredit=0;
                        $trxCount=0;
                        foreach($data['remo'] as $remo): 
                        
                    ?>
                        <tr>
                            <td><?=$this->dmy($remo['tanggal']);?></td>
                            <td>
                                <div class='trxNote'><?=$remo['arti'];?></div>
                                <div><?=$remo['keterangan'];?></div>
                            </td>
                            <td class='text-right'><?=number_format($remo['debet'],2,',','.');?></td>
                            <td class='text-right'><?=number_format($remo['kredit'],2,',','.');?></td>
                        </tr>
                    <?php 
                        $totalDebet+=$remo['debet'];
                        $totalKredit+=$remo['kredit'];
                        $trxCount+=1;
                        endforeach; 
                    ?>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr class='bg-summary'>
                        <td colspan="2">Jumlah Keseluruhan</td>
                        <td class="text-right font-weight-bold"><?=number_format($totalDebet,2,',','.');?></td>
                        <td class="text-right font-weight-bold"><?=number_format($totalKredit,2,',','.');?></td>
                    </tr>
                    <tr class='bg-result'>
                        <td colspan="3">Saldo Bulan ini</td>
                        <?php $saldo = $totalDebet - $totalKredit; ?>
                        <td class="text-right font-weight-bold"><?=number_format($saldo,2,',','.');?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Banyaknya transaksi : <?=$trxCount;?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div class="page-title">
            <h3>Rekap Tahun Berjalan</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Total Debet</th>
                        <td class="text-right"><?=number_format($data['reta']['debet'],2,',','.');?></td>
                    </tr>
                    <tr>
                        <th>Total Kredit</th>
                        <td class="text-right"><?=number_format($data['reta']['kredit'],2,',','.');?></td>
                    </tr>
                    <tr>
                        <th>Saldo</th>
                        <td class="text-right"><?=number_format($data['reta']['saldo'],2,',','.');?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3>Rekapitulasi Menurut Pos Biaya</h3>
        <table class="table table-striped table-bordered table-sm">
            <thead>
                <tr>
                    <th>Kode Pos</th>
                    <th>Pos Biaya</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $jdeb = 0; $jkre=0;
                foreach($data['repo'] as $repo ): 
                ?>
                <tr>
                    <td><?=$repo['kodePos'];?></td>
                    <td><?=$repo['arti'];?></td>
                    <td class='text-right'><?=number_format($repo['debet'],0,',','.');?></td>
                    <td class='text-right'><?=number_format($repo['kredit'],0,',','.');?></td>
                </tr>
                <?php
                $jdeb+=$repo['debet']; 
                $jkre+=$repo['kredit']; 
                endforeach; 
                ?>
                <tr class="font-weight-bold">
                    <td colspan="2" class='text-right pr-3'>Jumlah Keseluruhan</td>
                    <td class="text-right"><?=number_format($jdeb,0,',','.'); ?></td>
                    <td class="text-right"><?=number_format($jkre,0,',','.'); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php $this->view('template/bs4js');?>
<script>

$('#rkBulan').on('change',function(){
    window.location="<?=BASEURL;?>Bukukas/rekap/"+this.value;
})

</script>