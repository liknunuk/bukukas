<div class="row bg-dark">
  <div class="col-sm-12 py-0">
    <nav class="navbar navbar-expand-sm navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">The Pikas</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <?php $this->view('home/sysnav'); ?>
          <li class="nav-item <?= $this->isActive('index');?>">
            <a class="nav-link" href="<?=BASEURL;?>User">User <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item <?= $this->isActive('tambah');?>">
            <a class="nav-link" href="<?=BASEURL;?>User/baru">Tambah</a>
          </li>

          <?php $this->view('home/adminmenu'); ?>
          <!--
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          -->
        </ul>
        <div class="form-inline my-2 my-lg-0">
          <input id="namauser" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button id="cariuser" class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
        </div>
        <ul class="navbar-nav">
          <li class="nav-item"><a href="<?=BASEURL;?>Home/metusikah" class="nav-link">
            <i class="fas fa-sign-out-alt bg-danger" style="color:white;"></i>
          </a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>