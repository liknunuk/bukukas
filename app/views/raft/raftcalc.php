<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perhitungan Biaya Rafting</title>
    <style>
    th { text-align: left; padding-left: 15px; width: 150px;}
    table { font-family: monospace; font-size: 10px;}
    @media print{
        .back { display:none ;}
    }
    </style>
</head>
<body onload=window.print();>
    <h4>Kalkulasi Biaya Rafting</h4>
    <h4>Route <?=$data['calc']['trip'];?> Jumlah <?=$data['calc']['pax'];?> Orang</h4>
    <table>
        <tbody>
        <!-- Komponan A -->
            <tr>
                <th><?=$data['item']['a1'];?></th>
                <td align="right"><?=$data['calc']['qty_a1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_a1'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['a2'];?></th>
                <td align="right"><?=$data['calc']['qty_a2'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_a2'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['a3'];?></th>
                <td align="right"><?=$data['calc']['qty_a3'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_a3'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['a4'];?></th>
                <td align="right"><?=$data['calc']['qty_a4'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_a4'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtA'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['b1'];?></th>
                <td align="right"><?=$data['calc']['qty_b1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_b1'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtB'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['c1'];?></th>
                <td align="right"><?=$data['calc']['qty_c1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_c1'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['c2'];?></th>
                <td align="right"><?=$data['calc']['qty_c2'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_c2'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtC'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['d1'];?></th>
                <td align="right"><?=$data['calc']['qty_d1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_d1'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['d2'];?></th>
                <td align="right"><?=$data['calc']['qty_d2'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_d2'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['d3'];?></th>
                <td align="right"><?=$data['calc']['qty_d3'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_d3'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['d4'];?></th>
                <td align="right"><?=$data['calc']['qty_d4'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_d4'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtD'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['e1'];?></th>
                <td align="right"><?=$data['calc']['qty_e1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_e1'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['e2'];?></th>
                <td align="right"><?=$data['calc']['qty_e2'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_e2'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['e3'];?></th>
                <td align="right"><?=$data['calc']['qty_e3'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_e3'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['e4'];?></th>
                <td align="right"><?=$data['calc']['qty_e4'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_e4'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtE'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['f1'];?></th>
                <td align="right"><?=$data['calc']['qty_f1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f1'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f2'];?></th>
                <td align="right"><?=$data['calc']['qty_f2'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f2'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f3'];?></th>
                <td align="right"><?=$data['calc']['qty_f3'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f3'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f4'];?></th>
                <td align="right"><?=$data['calc']['qty_f4'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f4'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f5'];?></th>
                <td align="right"><?=$data['calc']['qty_f5'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f5'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f6'];?></th>
                <td align="right"><?=$data['calc']['qty_f6'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f6'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f7'];?></th>
                <td align="right"><?=$data['calc']['qty_f7'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f7'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f8'];?></th>
                <td align="right"><?=$data['calc']['qty_f8'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f8'],2,',','.');?></td>
            </tr>
            <tr>
                <th><?=$data['item']['f9'];?></th>
                <td align="right"><?=$data['calc']['qty_f9'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_f9'],2,',','.');?></td>
            </tr>
            <tr>
                <td align="right" colspan="3">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtF'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <th><?=$data['item']['g1'];?></th>
                <td align="right"><?=$data['calc']['qty_g1'];?></td>
                <td>x</td>
                <td align="right"><?=number_format($data['calc']['cost_g1'],2,',','.');?></td>
            </tr>
            <tr>
                <td colspan="3" align="right">Sub Total</td>
                <td align="right"><?=number_format($data['calc']['subtG'],2,',','.');?></td>
            </tr>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <td colspan="3" align="right"><strong>Total</strong></td>
                <td align="right"><?=number_format($data['calc']['totale'],2,',','.');?></td>
            </tr>
            <tr>
                <td colspan="3" align="right"><strong>Grand Total</strong></td>
                <td align="right"><?=number_format($data['calc']['grandTotal'],2,',','.');?></td>
            </tr>
        </tbody>
    </table>
<div class="back"><a href="<?=BASEURL;?>Rafting">Kembali</a></div>
</body>
</html>