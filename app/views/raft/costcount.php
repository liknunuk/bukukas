<div class="row">
    <div class="col-md-8 mx-auto mt-5">    
        <form action="<?=BASEURL;?>Rafting/write" method="post">
            <input type="hidden" name="trip" value="<?=$data['route'];?>">
            <input type="hidden" name="pax" value="<?=$data['pax'];?>">

            <h4>Trip <?=$data['route'];?> Jumlah <?=$data['pax'];?> Orang</h4>
            <?php $tabIndex=1;?>
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <td colspan="5" class="text-center"><h3>Tarif Dasar</h3></td>
                    </tr>
                    <tr>
                        <th>No.</th>
                        <th>Komponen</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th width='200'>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Komponen A -->
                    
                    <?php $a=1; foreach($data['cost']['A'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$a;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input type="number" class="form-control compA text-right" id="qty_a<?=$a;?>" name="qty_a<?=$a;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compA text-right" id="cost_a<?=$a;?>" name="cost_a<?=$a;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $a++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtA"  id="subtA" class="form-control text-right" readonly></td>
                        </tr>

                    <!-- Komponen B -->
                        
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Dokumentasi</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $b=1; $tabIndex++; foreach($data['cost']['B'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$b;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compB text-right" id="qty_b<?=$b;?>" name="qty_b<?=$b;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input type="number" class="form-control compB text-right" id="cost_b<?=$b;?>" name="cost_b<?=$b;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $b++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtB"  id="subtB" class="form-control text-right" readonly></td>
                        </tr>
        
                    <!-- Komponen C -->
                
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Transportasi</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $c=1; foreach($data['cost']['C'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$c;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compC text-right" id="qty_c<?=$c;?>" name="qty_c<?=$c;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input type="number" class="form-control compC text-right" id="cost_c<?=$c;?>" name="cost_c<?=$c;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $c++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtC"  id="subtC" class="form-control text-right" readonly></td>
                        </tr>
        
                    <!-- Komponen D -->
                    
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Minuman</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $c=1; foreach($data['cost']['D'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$c;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compD text-right" id="qty_d<?=$c;?>" name="qty_d<?=$c;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input type="number" class="form-control compD text-right" id="cost_d<?=$c;?>" name="cost_d<?=$c;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $c++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtD"  id="subtD" class="form-control text-right" readonly></td>
                        </tr>
        
        
                    <!-- Komponen E -->
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Personalia</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $c=1; foreach($data['cost']['E'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$c;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compE text-right" id="qty_e<?=$c;?>" name="qty_e<?=$c;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input type="number" class="form-control compE text-right" id="cost_e<?=$c;?>" name="cost_e<?=$c;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $c++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtE"  id="subtE" class="form-control text-right" readonly></td>
                        </tr>
        
        
                    <!-- Komponen F -->
                        
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Snack &amp; Meal</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $c=1; foreach($data['cost']['F'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$c;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compF text-right" id="qty_f<?=$c;?>" name="qty_f<?=$c;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input type="number" class="form-control compF text-right" id="cost_f<?=$c;?>" name="cost_f<?=$c;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $c++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtF"  id="subtF" class="form-control text-right" readonly></td>
                        </tr>
        
        
                    <!-- Komponen G -->
                    </tbody>
                    <thead>
                        <tr>
                            <td colspan="5" class="text-center"><h3>Share Pikas</h3></td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Komponen</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th width='200'>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $c=1; foreach($data['cost']['G'] as $par=>$val ): ?>
                        <tr>
                            <td class="text-center"><?=$c;?>.</td>
                            <td width='200'><?=$par;?></td>
                            <td>
                                <input type="number" class="form-control compG text-right" id="qty_g<?=$c;?>" name="qty_g<?=$c;?>" value="<?=$val['qty'];?>">
                            </td>
                            <td>
                                <input tabIndex=<?=$tabIndex;?> type="number" class="form-control compG text-right" id="cost_g<?=$c;?>" name="cost_g<?=$c;?>" value="<?=$val['cost'];?>">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php $c++; $tabIndex++; endforeach ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' >Sub Total</td>
                            <td><input type="number" name="subtG"  id="subtG" class="form-control text-right" readonly></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' class='text-right'>Total</td>
                            <td><input type="number" name="totale"  id="totale" class="form-control text-right" readonly></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan='3' class='text-right'>Grand Total</td>
                            <td><input type="number" name="grandTotal"  id="grandTotal" class="form-control text-right" readonly></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center">
                                <button type="submit" class="btn btn-primary">Catat!</button>
                            </td>
                        </tr>        
                </tbody>
            </table>
        </form>

    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
let i=0, subTotalA=0; subTotalB=0; subTotalC=0; subTotalD=0; subTotalE=0; subTotalF=0; subTotalG=0;
let totale , grandTotal;

// ( $('#qty_x').val() * $('#cost_x').val() )
// basic charge
$('.compA').on('change',function(){
    let subtA=0;
    subtA = ($('#qty_a1').val() * $('#cost_a1').val() ) + ($('#qty_a2').val() * $('#cost_a2').val() ) + ($('#qty_a3').val() * $('#cost_a3').val() ) - ($('#qty_a4').val() * $('#cost_a4').val() );
    
    subTotalA = subtA;
    $('#subtA').val( subtA );
})

// documentation
$('.compB').on('change',function(){
    let subtB=0;
    subtB = ( $('#qty_b1').val() * $('#cost_b1').val() );
    subTotalB = subtB;
    $('#subtB').val( subtB );
})

// transport
$('.compC').on('change',function(){
    let subtC=0;
    subtC = ( $('#qty_c1').val() * $('#cost_c1').val() ) + ( $('#qty_c2').val() * $('#cost_c2').val() );
    subTotalC = subtC;
    $('#subtC').val( subtC );
})

// drink
$('.compD').on('change',function(){
    let subtD=0;
    let i = 1;
    for( i = 1 ; i<=4 ; i++ ){
        subtD += ( $('#qty_d'+i).val() * $('#cost_d'+i).val() );
    }
    subTotalD = subtD;
    $('#subtD').val( subtD );
})

// crew
$('.compE').on('change',function(){
    let subtE=0;
    let i = 1;
    for( i = 1 ; i<=4 ; i++ ){
        subtE += ( $('#qty_e'+i).val() * $('#cost_e'+i).val() );
    }
    subTotalE = subtE;
    $('#subtE').val( subtE );
})

// meal and snack
$('.compF').on('change',function(){
    let subtF=0;
    let i = 1;
    for( i = 1 ; i<=9 ; i++ ){
        subtF += ( $('#qty_f'+i).val() * $('#cost_f'+i).val() );
    }
    subTotalF = subtF;
    $('#subtF').val( subtF );
})

// pikas share
$('.compG').on('change',function(){
    let subtG=0;
    subtG = ( $('#qty_g1').val() * $('#cost_g1').val() );
    subTotalG = subtG;
    $('#subtG').val( subtG );

    totale = subTotalB + subTotalC + subTotalD + subTotalE + subTotalF + subTotalG;
    grandTotal =  subTotalA - totale;
    
    $('#totale').val( totale );
    $('#totale').css( 'font-weight' , 'bold' );
    $('#grandTotal').val( grandTotal );
    $('#grandTotal').css( 'font-weight' , 'bold' );

})
</script>