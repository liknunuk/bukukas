<?php
$tempDir =  __DIR__ . '/mpdfTemp';
$pdfDir = dirname(__DIR__,2).'/mpdf';
require_once $pdfDir . '/vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(['format' => 'A4','tempDir'=>$tempDir]);



$bookingId=$data['booking']['bookingId'];
$price=number_format($data['booking']['price'],2,',','.');
$billing = number_format($data['booking']['billing'],2,',','.'); 
$pic = $data['booking']['namaPIC'];
$discount = number_format($data['booking']['discount'],2,',','.');
$persons = $data['booking']['jumlahPerson'];
$budget = $data['booking']['billing'] - $data['booking']['discount'];
$budget = number_format($budget,2,',','.');
$paket = $data['booking']['namaPaket'];
$tanggal = $data['booking']['tanggalMulai'];

$invoice=<<<EOL
<h1 style="text-align:center; margin-top:25px; margin-bottom:0;">The Pikas Adventure Banjarnegara</h1>
<h3 style="text-align:center; margin:0;">Invoice # {$bookingId}</h3>
<br/>
<p style="margin:0;">Kepada Yth. : {$pic}</p>
<p style="margin:0 0 25 0;">Di: {$data['booking']['alamat']}</p>

<table width="100%" border="1" cellspacing="0" cellpadding="5">
    <tbody>
        <tr>
            <td>Paket Rafting</td>
            <td width="150">Tarif / Pax</td>
            <td width="160" align="right">{$price}</td>
        </tr>
        <tr>
            <td>{$paket} - {$persons} pax</td>
            <td>Total Harga</td>
            <td align="right">{$billing}</td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>Diskon</td>
            <td align="right">{$discount}</td>
        </tr>
        <tr>
            <td>{$tanggal}</td>
            <td>Total Tagihan</td>
            <td align="right">{$budget}</td>
        </tr>
    </tbody>
</table>
EOL;
// echo $invoice;

$mpdf->WriteHTML($invoice);

// Output a PDF file directly to the browser
$mpdf->Output("{$data['booking']['bookingId']}.pdf", 'I');
?>
