<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="page-title">
            <h3>Pembayaran Rafting</h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 mx-auto">
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th>Booking Id</th>
                    <th>Nama</th>
                    <th>Tanggal</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($data['booking'] as $booking): ?>
                <tr>
                    <td>
                    <a href="#" class="btn btn-primary bookId"><?=$booking['bookingId'];?></a>
                    </td>
                    <td><?=$booking['namaPIC'];?></td>
                    <td><?=$this->dmy($booking['tanggalMulai']);?></td>
                    <td><?=$booking['jumlahPerson'];?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <form action="<?=BASEURL;?>Rafting/payment" method="post" class="form-horizontal">
        <input type="hidden" name="bookingId">
            <div class="form-group row">
                <label for="rftTanggal" class="col-md-4">Tanggal</label>
                <div class="col-md-8">
                    <input type="date" name="tanggal" id="rftTanggal" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="rftTGroup" class="col-md-4">Group</label>
                <div class="col-md-8">
                    <input date="text" name="group" id="rftTGroup" class="form-control" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="rftPax" class="col-md-4">Jumlah</label>
                <div class="col-md-8">
                    <input date="number" name="pax" id="rftPax" class="form-control" readonly min="1" value="1">
                </div>
            </div>

            <div class="form-group row">
                <label for="rftTrip" class="col-md-4">Trip</label>
                <div class="col-md-8">
                    <select name="trip" id="rftTrip" class="form-control">
                        <option value="">Pilih Trip</option>
                        <?php 
                        $prices=[];
                        foreach($data['paket'] as $paket): 
                        ?>
                        <option value="<?=$paket['paketRaftingId'];?>">[ <?=$paket['paketRaftingId'];?> ] - <?=$paket['namaPaket'];?></option>
                        <?php
                        $paketId=$paket['paketRaftingId'];
                        $prices[$paketId]=$paket['price'];
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="rftPrice" class="col-md-4">Tarif / Pax</label>
                <div class="col-md-8">
                    <input type="number" name="price" id="rftPrice" class="form-control text-right" readonly>
                </div>
            </div>
            <div class="form-group d-flex justify-content-end px-3">
                <input type="submit" value="Proses" class="btn btn-success">
            </div>
        </form>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
let prices = <?=json_encode($prices,1);?>;
$('.bookId').on('click',function(){
    let bookId =  this.text ;
    $.ajax({
        dataType: 'json',
        url : '<?=BASEURL;?>Rafting/raftDetail/'+bookId,
        success : function(resp){
            $('#rftTanggal').val( resp.tanggalMulai );
            $('#rftTGroup').val( resp.namaPIC );
            $('#rftPax').val( resp.jumlahPerson );
            $("input[name='bookingId']").val(bookId);
        }
    })
})

$('#rftTrip').change( function(){
    let paket = this.value;
    let price = prices[paket];
    $('#rftPrice').val(price);
})
</script>