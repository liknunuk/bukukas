<div class="row">
    <div class="col-lg-12">
        <div class="page-title">
            <h3>Tagihan Aktif</h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">

        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>No. Booking</th>
                        <th>Tanggal</th>
                        <th>Group/PIC</th>
                        <th>Paket</th>
                        <th>Jumlah Harga</th>
                        <th>Diskon</th>
                        <th>Tagihan</th>
                        <th class="text-center"><i class="fa fa-gear"></i></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['billing'] as $bill):?>
                    <tr>
                        <td><?=$bill['bookingId'];?></td>
                        <td><?=$bill['tanggalMulai'];?></td>
                        <td>
                        <?=$bill['namaPIC'];?><br>
                        <small><span class="jmPerson"><?=$bill['jumlahPerson'];?></span> Personel</small>
                        </td>
                        <td>
                        <?=$bill['namaPaket'];?><br/>
                        <small>Rp <?=number_format($bill['price'],0,',','.');?></small>
                        </td>
                        <td class='text-right'><?=number_format($bill['billing'],0,',','.');?></td>
                        <td class='text-right'><?=number_format($bill['discount'],0,',','.');?></td>
                        <td class='text-right'>
                        <?php
                        $totalTagihan = $bill['billing'] - $bill['discount'];
                        echo number_format($totalTagihan,0,',','.');
                        ?>
                        </td>
                        <td class="text-center">
                            <a href="javascript:void(0)"><i class="fa fa-percent btn btn-primary"></i></a>
                            <a href="javascript:void(0)"><i class="fa fa-edit btn btn-primary"></i></a>
                            <a href="javascript:void(0)"><i class="fa fa-trash btn btn-danger"></i></a>
                            <a href="<?=BASEURL .'Rafting/expenditure/' . $bill['bookingId'];?>"><i class="fa fa-calculator btn btn-success"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>        
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12"></div>
</div>
<!-- modals -->
<!-- Modal Diskon: mdDiskon -->
<div class="modal" tabindex="-1" role="dialog" id="mdDiskon">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Diskon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>Rafting/setDiskon" method="post" class="form-horizontal">
            <div class="form-group row">
                <label for="mddBookingId" class="col-sm-3">Booking ID</label>
                <div class="col-sm-9">
                    <input type="text" name="bookingId" id="mddBookingId" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="mddTarif" class="col-sm-3">Tarif</label>
                <div class="col-sm-9">
                    <input type="text" disable id="mddTarif" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="mddDiskon" class="col-sm-3">Diskon</label>
                <div class="col-sm-4">
                    <input type="number" name="discount" id="mddDiskon" class="form-control" min=0 required>
                </div>
                <div class="col-sm-5">
                    x <span id="mddPersonelLabel"></span>
                    <input type="hidden" name="jmPersonel" id="mddPersonel">
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- modals -->
<?php $this->view('template/bs4js'); ?>
<script>
$('.fa-percent').on('click',function(){
    let bookingId=$(this).parent().parent().parent().children('td:nth-child(1)').text();
    let jmlPerson=$(this).parent().parent().parent().children('td:nth-child(3)').children('small').children('span.jmPerson').text();
    let tarif=$(this).parent().parent().parent().children('td:nth-child(4)').children('small').text();
    // console.log(bookingId , jmlPerson);
    $('#mdDiskon').modal('show');
    $('#mddPersonelLabel').text(jmlPerson);
    $('#mddPersonel').val(jmlPerson);
    $('#mddTarif').val(tarif);
    $('#mddBookingId').val(bookingId);

})

$('.fa-edit').on('click',function(){
    let bookingId=$(this).parent().parent().parent().children('td:nth-child(1)').text();
    let tenan = confirm('Lakukan perhitungan ulang ?');
    if( tenan == true ){
        $.post("<?=BASEURL;?>Rafting/recalculate" , { bookingId:bookingId} , function(resp){
            // console.log(resp);
            if( resp == '1' ){
                window.location="<?=BASEURL;?>Rafting";
            }
        })
    }
})

$('.fa-trash').on('click',function(){
    let bookingId=$(this).parent().parent().parent().children('td:nth-child(1)').text();
    let tenan = confirm('Order / Booking dibatalkan?');
    if( tenan == true ){
        $.post("<?=BASEURL;?>Rafting/woeroeng" , { bookingId:bookingId},function(resp){
            if(resp == '1'){
                window.location="<?=BASEURL;?>Rafting";
            }
        })
    }
})
</script>