<?php
// -- userId , fullName , email , tempPassowrd , isadmin
?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="page-title">
            <h3>
            Pengguna Baru
            </h3>
        </div>
        <form action="<?=BASEURL;?>User/setUser" method="post" class='form-horizontal'>
            
            <div class="form-group row">
                <label class="col-md-3" for="personEmail">Alamat Email</label>
                <div class="col-md-9">
                    <input type="email" id="personEmail" name="email" class="form-control" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3" for="personFullName">Nama Lengkap</label>
                <div class="col-md-9">
                    <input type="text" id="personFullName" name="fullName" class="form-control" required maxlength="40">
                </div>
            </div>
            
            <div class="form-group row ">
                <label class="col-md-3" for="personIsAdmin">Administratur</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="form-check col-sm-6">
                            <input class="form-check-input" type="radio" value="1" id="personIsAdmin" name="isAdmin">
                            <label class="form-check-label" for="pesonIsAdmin">
                                Administratur
                            </label>
                        </div>

                        <div class="form-check col-sm-6">
                            <input class="form-check-input" type="radio" value="0" id="personIsAdmin" name="isAdmin" checked>
                            <label class="form-check-label" for="pesonIsAdmin">
                                User Reguler
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3" for="personSandi1">Kata Sandi</label>
                <div class="col-md-9">
                    <input type="password" id="personSandi1" name="sandinya" class="form-control" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3" for="personSandi2">Ulangi Sandi</label>
                <div class="col-md-9">
                    <input type="password" id="personSandi2" class="form-control" required>
                </div>
            </div>
            <div class="form-group row text-center" id="passCheck">
            </div>
            <div class="form-group row d-flex justify-content-end">
                <button type="submit" id="btnSubmit" disabled class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $("#personSandi2").on('keyup', function(){
        let p1,p2;
        p1=$("#personSandi1").val();
        p2=$("#personSandi2").val();

        
        if( p2.length == p1.length ){

            if( p2 != p1 ){
                $('#passCheck').addClass('bg-danger');
                $('#passCheck').text('Kata Sandi Tidak Sama!');
                $("#personSandi1").val('');
                $("#personSandi2").val('');
            }else{
                $('#btnSubmit').prop('disabled',false);
            }

        }
    })

    $("#personSandi1").on('focus',function(){
       $('#passCheck').text('');
       $('#passCheck').removeClass('bg-danger');
    })
</script>