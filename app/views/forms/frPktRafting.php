<form action="<?=BASEURL;?>Rafting/setPaket" method="post">
    <input type="hidden" name="modus" value="anyar">
<!-- --  paketRaftingId , namaPaket , deskripsi , price , miniper -->
    <div class="form-group">
        <label for="prfPaketId">Kode Paket</label>
        <input type="text" id="prfPaketId" name="paketRaftingId" class="form-control">
    </div>
    
    <div class="form-group">
        <label for="prfNama">Nama Paket</label>
        <input type="text" id="prfNama" name="namaPaket" class="form-control">
    </div>

    <div class="form-group">
        <label for="prfDeskprisi">Deskripsi Singkat</label>
        <input type="text" id="prfDeskprisi" name="deskripsi" class="form-control">
    </div>

    <div class="form-group">
        <label for="prfHarga"></label>
        <input type="text" id="prfHarga" name="price" class="form-control">
    </div>

    <div class="form-group">
        <label for="prfMinimumPerson">Minimum Person</label>
        <input type="text" id="prfMinimumPerson" name="miniper" class="form-control">
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>