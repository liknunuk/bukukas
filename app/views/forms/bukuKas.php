
<!-- -- kasId , kodePos , tanggal , keterangan , debet, kredit -->
<form action="<?=BASEURL . $data['controller'];?>/simpan" method="post" class='form-horizontal' id="frBukukas">
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkKasId">Nomor Pencatatan</label>
        <div class="col-sm-9">
            <input type="text" id="bkKasId" name="kasId" class="form-control" readonly placeHolder="dibuat otomatis">
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkKodePos">Kode Pos</label>
        <div class="col-sm-9">
            <input type="number" id="bkKodePos" name="kodePos" class="form-control" required>
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkTanggal">Tanggal</label>
        <div class="col-sm-9">
            <input type="date" id="bkTanggal" name="tanggal" class="form-control" value="<?=date('Y-m-d');?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkKeterangan">Keterangan/Berita</label>
        <div class="col-sm-9">
            <textarea rows='3' id="bkKeterangan" name="keterangan" class="form-control" required></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkDebet">Jumlah Masuk</label>
        <div class="col-sm-9">
            <input type="number" id="bkDebet" name="debet" class="form-control" value="0" min="0">
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-3" for="bkKredit">Jumlah Keluar</label>
        <div class="col-sm-9">
            <input type="number" id="bkKredit" name="kredit" class="form-control" value="0" min="0">
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>