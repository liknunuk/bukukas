<div class="row">
    <div class="col-md-6 mx-auto mt-3">
        <div class="page-title">
            <h3>PENGGANTIAN PASSWORD</h3>
        </div>
        <form action="<?=BASEURL;?>User/chPass" method="post">
            
            <div class="form-group">
                <label for="password1">User Id</label>
                <input type="text" name="userId" class="form-control" readonly value="<?=$data['userId'];?>">
            </div>
            
            <div class="form-group">
                <label for="password1">Kata Sandi</label>
                <input type="password" id="password1" name="sandinya" class="form-control" required>
            </div>
            
            <div class="form-group">
                <label for="password2">Ulangi Kata Sandi</label>
                <input type="password" id="password2" required class="form-control">
            </div>
            <div class="passCheck"></div>
            <div class="form-group d-flex justify-content-end">
                <button id="btnSubmit" disabled type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>    
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$('#password2').on('keyup',function(){
    let p1, p2;
    p1 = $('#password1').val();
    p2 = $('#password2').val();
    if( p2.length == p1.length ){
        if( p2 == p1 ){
            $('#btnSubmit').prop('disabled',false);
            $('.passCheck').removeClass('bg-danger');
            $('.passCheck').text('');
        }else{
            $('.passCheck').addClass('bg-danger');
            $('.passCheck').text('Password Tidak Sama');
            $('#password1').val('');
            $('#password1').focus();
            $('#password2').val('');
        }
    }
})
</script>