<div class="container-fluid login-page">
  <div class="row" style="height:90vh">
    <div class="col-lg-12">
        <div id="login-box-wrapper">
            <?php Alert::sankil(); ?>
            <div id="login-banner"><h4>Login Pengguna</h4></div>
            <form action="<?=BASEURL;?>Home/auth" method="post">
                <div class="form-group">
                    <label for="personEmail">Email</label>
                    <input type="email" id="personEmail" name="email" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="personPassword">Kata Sandi</label>
                    <input type="password" id="personPassword" name="password" class="form-control">
                </div>

                <div class="form-group d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>


