<div class="row bg-dark">
  <div class="col-sm-12 py-0">
    <nav class="navbar navbar-expand-sm navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">The Pikas</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

          <?php $this->view('home/sysnav'); ?>

          <li class="nav-item <?= $this->isActive('index');?>">
            <a class="nav-link" href="<?=BASEURL;?>Bukukas">Booking <span class="sr-only">(current)</span></a>
          </li>
                 
          <!-- 
            <li class="nav-item">
            <a href="<?=BASEURL;?>User" class="nav-link">User</a>
          </li> 
          -->

          <?php $this->view('home/adminmenu'); ?>

          <!--
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          -->
        </ul>
        <form class="form-inline my-2 my-lg-0" action="<?=BASEURL;?>Bukukas/caritrx" method="post">
          <input type="hidden" name="col" value="keterangan">
          <input name="key" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
        </form>
        <ul class="navbar-nav">
          <li class="nav-item"><a href="<?=BASEURL;?>Home/metusikah" class="nav-link">
            <i class="fas fa-sign-out-alt bg-danger" style="color:white;"></i>
          </a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>