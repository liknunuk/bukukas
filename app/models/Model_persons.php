<?php
class Model_persons
{
    private $table = "pikasPerson";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        
        $katasandi = $this->setPassword($data['email'],$data['sandinya']);
        $sql = "INSERT INTO $this->table SET fullName=:fullName , email=:email , tempPassword=:katasandi , isAdmin=:isAdmin ";
        $this->db->query($sql);
        $this->db->bind('fullName',$data['fullName']);
        $this->db->bind('email',$data['email']);
        $this->db->bind('isAdmin',$data['isAdmin']);
        $this->db->bind('katasandi',$katasandi);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        // $katasandi = $this->setPassword($data['email'] , $data['sandinya']);
        $sql = "UPDATE $this->table SET fullName=:fullName  WHERE userId=:userId";
        $this->db->query($sql);
        $this->db->bind('fullName',$data['fullName']);
        $this->db->bind('userId',$data['userId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE userId=:userId ";
        $this->db->query($sql);
        $this->db->bind('userId',$data['userId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn - 1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY fullName LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE userId=:userId ";
        $this->db->query($sql);
        $this->db->bind('userId',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    
    public function something($data){
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->bind('xxx',$xxx);
        return $this->db->resultSet();
    }

    public function login($email,$password){
        $sandi = $this->setPassword($email,$password);
        $sql = "SELECT * FROM $this->table WHERE tempPassword = :sandi";
        $this->db->query($sql);
        $this->db->bind('sandi',$sandi);
        return $this->db->resultOne();
    }

    public function chPasswd($data){
        $email = $this->getemail($data['userId']);
        $sandi = $this->setPassword($email,$data['sandinya']);
        $sql = "UPDATE $this->table SET tempPassword=:sandi WHERE userId=:userId";
        $this->db->query($sql);
        $this->db->bind('userId',$data['userId']);
        $this->db->bind('sandi',$sandi);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function switchAdmin($data){
        $sql = "UPDATE $this->table SET isadmin=:isadmin WHERE userId=:userId";
        $this->db->query($sql);
        $this->db->bind('isadmin',$data['isadmin']);
        $this->db->bind('userId',$data['userId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function cariUser($fullName){
        $sql = "SELECT * FROM $this->table WHERE fullName LIKE :fullName LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('fullName',"%{$fullName}%");
        return $this->db->resultSet();
    }

    public function authenticate($data){
        $tempPassword = $this->setPassword($data['email'],$data['password']);
        $sql = "SELECT * FROM $this->table WHERE tempPassword=:tempPassword LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('tempPassword',$tempPassword);
        return $this->db->resultOne();
    }
    
    // PRIVATE FUNCTIONS
    private function setPassword($email,$sandinya){
        return md5("_{$email}##*##{$sandinya}_");
    }

    private function getemail($uid){
        $sql = "SELECT email FROM $this->table WHERE userId=:userId";
        $this->db->query($sql);
        $this->db->bind('userId',$uid);
        $output = $this->db->resultOne();
        return $output['email'];
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/