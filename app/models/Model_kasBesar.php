<?php
class Model_kasBesar
{
    private $table = "kasBesar";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //
    //  kasId , kodePos , tanggal , keterangan , debet, kredit
    // INSERTION
    public function tambah($data){

        $sql = "INSERT INTO $this->table SET kodePos=:kodePos , tanggal=:tanggal , keterangan=:keterangan , debet=:debet , kredit=:kredit";
        $this->db->query($sql);
        $this->db->bind('kodePos',$data['kodePos']);
        $this->db->bind('tanggal',$data['tanggal']);
        $this->db->bind('keterangan',$data['keterangan']);
        $this->db->bind('debet',$data['debet']);
        $this->db->bind('kredit',$data['kredit']);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    // UPDATE
    //  kasId , kodePos , tanggal , keterangan , debet, kredit
    public function ngubah($data){
        $sql = "UPDATE $this->table SET kodePos=:kodePos , tanggal=:tanggal , keterangan:keterangan , debet=:debet , kredit=:kredit WHERE kasId=:kasId";
        $this->db->query($sql);
        $this->db->bind('kodePos',$data['kodePos']);
        $this->db->bind('tanggal',$data['tanggal']);
        $this->db->bind('keterangan',$data['keterangan']);
        $this->db->bind('debet',$data['debet']);
        $this->db->bind('kredit',$data['kredit']);
        $this->db->bind('kasId',$data['kasId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE kasId=:kasId";
        $this->db->query($sql);
        $this->db->bind('kasId',$data['kasId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY tanggal DESC LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE kasId=:kasId ";
        $this->db->query($sql);
        $this->db->bind('kasId',$kasId);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function transaksiHarian($tanggal){
        $sql = "SELECT * FROM $this->table WHERE tanggal=:tanggal ORDER BY kasId DESC";
        $this->db->query($sql);
        $this->db->bind('tanggal',$tanggal);
        return $this->db->resultSet();
    }

    public function rekapBulanan($bulan){
        $sql = "SELECT tanggal, kodePos , arti , keterangan , debet , kredit FROM kasBesar, chartOfAccount WHERE chartOfAccount.kode = kasBesar.kodePos && tanggal LIKE :bulan ORDER BY tanggal";
        $this->db->query($sql);
        $this->db->bind('bulan',"{$bulan}%");
        return $this->db->resultSet();
    }

    public function rekapPosBulanan($bulan){
        $sql = "SELECT kodePos, chartOfAccount.arti , SUM(debet) debet , SUM(kredit) kredit FROM kasBesar, chartOfAccount WHERE chartOfAccount.kode = kasBesar.kodePos && tanggal LIKE :bulan GROUP BY kodePos";

        // $sql = "SELECT tanggal, kodePos , arti , keterangan , debet , kredit FROM kasBesar, chartOfAccount WHERE chartOfAccount.kode = kasBesar.kodePos && tanggal LIKE :bulan ORDER BY tanggal";
        $this->db->query($sql);
        $this->db->bind('bulan',"{$bulan}%");
        return $this->db->resultSet();
    }

    public function rekapTahunan($tahun){
        $sql = "SELECT SUM(debet) debet , SUM(kredit) kredit , (SUM(debet) - SUM(kredit)) saldo FROM kasBesar WHERE tanggal LIKE :tahun";
        $this->db->query($sql);
        $this->db->bind('tahun',"{$tahun}%");
        return $this->db->resultOne();
    }

    public function cariTrx($kolom,$data){
        $sql = "SELECT kasBesar.* , chartOfAccount.arti FROM kasBesar , chartOfAccount WHERE $kolom LIKE :data && chartOfAccount.kode = kasBesar.kodePos";
        $this->db->query($sql);
        $this->db->bind('data',"%{$data}%");
        return $this->db->resultSet();
    }

}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/